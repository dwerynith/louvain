#!/usr/local/bin/python3

import sys

import igraph as ig
import louvain
import time


def cat_readme():
    """
    Useful to print the help string.
    """
    with open("README.md") as infile:
        for line in infile:
            if line:
                print(line, end="")


def get_opt(argv):
    """
    Parse program options.
    :param argv: sys.argv
    :return: filename, verbose, display, benchmark
    """
    v = False
    d = False
    b = False
    p = False
    if "-h" in argv:
        cat_readme()
        exit(0)
    if "-v" in argv:
        v = True
        print("- Verbose mode enabled.")
    if "-d" in argv:
        d = True
        print("- Display mode enabled.")
    if "-b" in argv:
        b = True
        print("- Benchmark mode enabled.")
    if "-p" in argv:
        p = True
        print("- Permute mode enabled.")
    return argv[1], v, d, b, p


def import_graph(filename, v):
    """
    Import file as graph.
    :param filename: file to parse
    :param v: verbose
    :return: the graph, importing time
    """
    begin = time.time()
    if verbose:
        print("Importing graph:")
    g = ig.Graph.Read_Edgelist(filename)
    if v:
        print("    * DONE")
    return g, time.time() - begin


def compute_louvain(g, v):
    """
    Compute Louvain.
    :param g: graph
    :param v: verbose
    :return: the partitions, compute time
    """
    begin = time.time()
    if v:
        print("Computing the partitions:")
    part = louvain.find_partition(g, louvain.ModularityVertexPartition)
    if v:
        print("    * DONE")
    return part, time.time() - begin


def print_bench(part, imp, comp):
    print(f"""
    Execution ended at: {time.ctime()}:
        - importing = {imp} s
        - computing = {comp} s
        - modularity = {part.quality()}
        - {part.summary()}
""")


def swap(g, v, reverse):
    """
    Modify the graph to try to enhance performances.
    :param v: verbose
    :param reverse: ascending or descending order
    :param g: the graph
    :return: modified graph
    """
    if v:
        print("Computing the rearranged graph:")
    begin = time.time()
    vertices = [(vert.index, vert.degree()) for vert in g.vs]
    graph = g.permute_vertices([i for (i, _) in sorted(vertices, key=lambda x: x[1], reverse=reverse)])
    if v:
        print("    * DONE")
    return graph, time.time() - begin


if __name__ == '__main__':
    file_path, verbose, display, bench, permute = get_opt(sys.argv)
    if bench:
        print(f"Start Execution : {time.ctime()}")

    G, import_time = import_graph(file_path, verbose)
    partition, compute_time = compute_louvain(G, verbose)

    if bench:
        print_bench(partition, import_time, compute_time)
    if display:
        begin_printing = time.time()
        ig.plot(partition)
        print_time = time.time() - begin_printing
        if bench:
            print(f"printing took: {print_time}")

    if permute:
        print("Permute with edges in ascending order.")
        G_asc, permute_time = swap(G, verbose, False)
        partition, new_compute_time = compute_louvain(G_asc, verbose)

        if bench:
            print_bench(partition, permute_time, new_compute_time)
            print(f"Modifying the graph gained: {compute_time - new_compute_time} s.")

        print("Permute with edges in descending order.")
        G_desc, permute_time = swap(G, verbose, True)
        partition, new_compute_time = compute_louvain(G_desc, verbose)

        if bench:
            print_bench(partition, permute_time, new_compute_time)
            print(f"Modifying the graph gained: {compute_time - new_compute_time} s.")
