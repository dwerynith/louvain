# Louvain

## Setting up:

    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt

## Execution:

    ./louvain-code.py path-to-graph [-h][-v][-d][-b][-p]

* -h: display this file
* -v: verbose
* -d: display graph at the end
* -b: benchmark each step
* -p: rearrange the graph

The graphs used can be found at:

    http://data.complexnetworks.fr/Diameter/